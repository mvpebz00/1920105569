@extends('layouts.app')

@section('content')
    <div class="container">
        @if (Auth()->user()->level == 'admin')
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Tambah Perusahaan</button>

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Perusahaan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/createPerusahaan" method="POST">
                        @csrf
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Nama Perusahaan:</label>
                        <input type="text" class="form-control" id="recipient-name" name="nama_perusahaan" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Alamat Perusahaan:</label>
                        <input type="text" class="form-control" id="recipient-name" name="alamat_perusahaan" required>
                    </div>
                        <input type="submit" value="Kirim" class="btn btn-primary">

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                </div>
            </div>
            </div>

        @endif
        <table class="table  table-inverse table-responsive">
            <thead class="thead-inverse|thead-default">
                <tr>
                    <th>Nama Perusahaan</th>
                    <th>Alamat Perusahaan</th>
                    @if (Auth()->user()->level == 'admin')
                        <th>Aksi</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                    @foreach ($data as $i)
                    <tr>
                        <td>{{ $i->nama_perusahaan }}</td>
                        <td>{{ $i->alamat_perusahaan }}</td>
                        @if (Auth()->user()->level == 'admin')
                            <td>
                                <a href="" class="btn btn-warning">Edit</a>
                                <a href="" class="btn btn-danger">Delete</a>
                            </td>
                        @endif
                        </tr>
                    @endforeach
                </tbody>
        </table>
     </div>
@endsection