@extends('layouts.pembimbing.dashboard')

@section('body')

    <div class="container">
    
                <table class="table table-bordered  table-responsive">
                        <tr>
                            <th rowspan="2">Tanggal</th>
                            <th >Kegiatan</th>
                            <th >Jam Kegiatan PRAKERIN</th>
                            <th >Keterangan</th>
                            <th >Aksi</th>
                        </tr>
                        <tbody>
                            @foreach ($data as $jurnal)
                                
                            <tr>
                                
                                <td>{{ $jurnal->tanggal }}</td>
                                <td><textarea name="" id="" cols="30" rows="5" readonly>{{ $jurnal->kegiatan }}</textarea></td>
                                <td>{{ $jurnal->jam_masuk }} - {{ $jurnal->jam_keluar }}</td>
                                <td>
                                    @if ($jurnal->keterangan == 'Sudah DI cek Oleh Pembimbing')
                                        <span class="btn btn-success">Sudah di cek oleh pembimbing</span>
                                    @endif
                                </td>
                                <td>
                                    @if ($jurnal->keterangan == null)
                                    <form action="/updateKeteranganJurnal" method="POST">
                                        @csrf
                                        <input type="hidden" name="keterangan" value="Sudah DI cek Oleh Pembimbing">
                                        <input type="hidden" name="id" value="{{ $jurnal->id }}">
                                        <input type="submit" value="Saya Sudah Cek" class="btn btn-primary">
                                    </form>
                                    @elseif ($jurnal->keterangan == 'Sudah DI cek Oleh Pembimbing')
                                        <button class="btn btn-primary" disabled>Saya Sudah Cek</button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                </table>
                <button onclick="kembali()" class="btn btn-danger">Kembali</button>
                <script>function kembali(){
                    window.history.back();
                }</script>
            </div>
        </div>
    </div>

    @endsection